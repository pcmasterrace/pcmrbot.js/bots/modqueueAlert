import { Service, Errors } from "moleculer";
import * as Sequelize from "sequelize";
import * as moment from "moment";
import * as yn from "yn";
import * as path from "path";

class ModqueueAlertsService extends Service {
	protected db: Sequelize.Sequelize;
	protected SlackAlert: Sequelize.Model<{channel: string, ts: string}, {}>;

	constructor (broker) {
		super(broker);

		this.parseServiceSchema({
			name: "bot.modqueueAlert",
			version: 1,
			dependencies: [
				{name: "reddit.post", version: 1},
				{name: "reddit.modqueue", version: 1},
				{name: "slack.web", version: 1}
			],
			actions: {
				checkModqueue: {
					name: "checkModqueue",
					handler: this.checkModqueue
				}
			},
			// @ts-ignore (bug introduced in v0.13)
			started: this.serviceStarted
		});
	}

	async checkModqueue() {
		let modqueue;
		try{
			modqueue = await this.broker.call("v1.reddit.modqueue.getModqueue", {
				subreddit: process.env.BOT_MODQUEUEALERT_SUBREDDIT
			});
		} catch (err) {
			this.logger.error("Reddit call returned error, please investigate. Restarting in 2 minutes...");
			setTimeout(() => this.broker.call("v1.bot.modqueueAlert.checkModqueue"), 2*60*1000);
			return;
		}

		if (modqueue.length >= (Number(process.env.BOT_MODQUEUEALERT_THRESHOLD) || 20)) {
			this.postSlackAlert(modqueue.length);
		} else {
			this.removeSlackAlert();
		}

		setTimeout(() => this.broker.call("v1.bot.modqueueAlert.checkModqueue"), 2*60*1000);
	}

	async postSlackAlert(unresolvedModqueue: number) {
		let existingAlert = await this.SlackAlert.findOne();
		if(existingAlert === null) {
			this.logger.debug("Slack alert does not exist, creating...");
			
			let messageString = `*Notice* <!channel>: There are ${unresolvedModqueue} items in the modqueue that need to be addressed: <https://www.reddit.com/r/mod/about/modqueue>`;
			
			let message = await this.broker.call("v1.slack.web.postMessage", {
				channel: process.env.BOT_MODQUEUEALERT_CHANNEL,
				message: messageString
			});
			
			await this.SlackAlert.create({
				channel: message.channel,
				ts: message.ts
			});
		} else {
			this.logger.debug("Slack alert already exists, updating...");
			let messageString = `*Notice* @channel: There are ${unresolvedModqueue} items in the modqueue that need to be addressed: <https://www.reddit.com/r/mod/about/modqueue>`;
			
			await this.broker.call("v1.slack.web.updateMessage", {
				channel: existingAlert.channel,
				ts: existingAlert.ts,
				message: messageString
			});
		}
	}
	
	async removeSlackAlert() {
		let existingAlert = await this.SlackAlert.findOne();
		if (existingAlert === null) {
			this.logger.debug("No action needed");
		} else {
			this.logger.debug("Slack alert exists, deleting...");
			
			let time = moment.utc();
			await this.broker.call("v1.slack.web.updateMessage", {
				channel: existingAlert.channel,
				ts: existingAlert.ts,
				message: `Queue was cleared at <!date^${time.unix()}^{time} on {date}|${time.format("LTS")} UTC on ${time.format("LL")}>`
			});

			await this.SlackAlert.destroy({where: {ts: existingAlert.ts}})
		}
	}

	async serviceStarted() {
		if(process.env.BOT_MODQUEUEALERT_CHANNEL === "") throw new Errors.MoleculerError("No modqueue alert Slack channel defined, you dingus");
		
		this.db = new Sequelize({
			dialect: process.env.BOT_MODQUEUEALERT_DB_DIALECT || undefined,
			database: process.env.BOT_MODQUEUEALERT_DB_NAME || undefined,
			host: process.env.BOT_MODQUEUEALERT_DB_HOST || undefined,
			username: process.env.BOT_MODQUEUEALERT_DB_USERNAME || undefined,
			password: process.env.BOT_MODQUEUEALERT_DB_PASSWORD || undefined,
			port: Number(process.env.BOT_MODQUEUEALERT_DB_PORT) || undefined,
			storage: process.env.BOT_MODQUEUEALERT_DB_PATH || undefined,
			operatorsAliases: false,
			logging: false
		});

		this.SlackAlert = this.db.import(path.join(__dirname, "./modqueueSlackAlert.model"));

		await this.db.sync({force: yn(process.env.BOT_MODQUEUEALERT_DB_DROP_TABLES_ON_START, {default: false})});

		setTimeout(() => this.broker.call("v1.bot.modqueueAlert.checkModqueue"), 2*40*1000);
	}
}

module.exports = ModqueueAlertsService;