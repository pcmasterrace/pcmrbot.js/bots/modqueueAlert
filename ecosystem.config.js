module.exports = {
	apps : [{
		name      : 'bot/modqueueAlert',
		script    : 'build/modqueueAlert.js',
		env: {
			// Either inject the values via environment variables or define them here
            TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
            BOT_MODQUEUEALERT_SUBREDDIT: process.env.BOT_MODQUEUEALERT_SUBREDDIT || "",
            BOT_MODQUEUEALERT_THRESHOLD: process.env.BOT_MODQUEUEALERT_THRESHOLD || "",
            BOT_MODQUEUEALERT_CHANNEL: process.env.BOT_MODQUEUEALERT_CHANNEL || "",
            DB_HOST: process.env.DB_HOST || "",
            DB_PORT: process.env.DB_PORT || "",
            DB_USERNAME: process.env.DB_USERNAME || "",
            DB_PASSWORD: process.env.DB_PASSWORD || "",
            DB_NAME: process.env.DB_NAME || ""
		}
	}]
};
